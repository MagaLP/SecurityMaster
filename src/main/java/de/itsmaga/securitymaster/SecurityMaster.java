package de.itsmaga.securitymaster;

import de.itsmaga.securitymaster.action.ActionManager;
import de.itsmaga.securitymaster.auth.AuthManager;
import de.itsmaga.securitymaster.commander.CommandManager;
import de.itsmaga.securitymaster.event.EventManager;
import de.itsmaga.securitymaster.file.ConfigurationFiles;
import de.itsmaga.securitymaster.logger.Logger;
import de.itsmaga.securitymaster.module.Module;
import de.itsmaga.securitymaster.module.ModuleLoader;
import de.itsmaga.securitymaster.network.NetworkManager;
import de.itsmaga.securitymaster.network.netty.NettyConnection;
import de.itsmaga.securitymaster.players.PlayerManager;
import de.itsmaga.securitymaster.utils.SecurityProfiler;
import lombok.Getter;
import lombok.Setter;


/**
 * Erstellt von ItsMaga
 */
@Getter
@Setter
public class SecurityMaster {

    @Getter @Setter private static SecurityMaster instance;
    private NetworkManager networkManager;
    private CommandManager commandManager;
    private PlayerManager playerManager;
    private ActionManager actionManager;
    private AuthManager authManager;
    private ConfigurationFiles configurationFiles;
    private EventManager eventManager;
    private ModuleLoader moduleLoader;



    /**
     * Servers
     */
    private NettyConnection nettyConnection;

    public SecurityMaster() {
        setInstance(this);
    }

    /**
     * Main method
     * @param args
     */
    public static void main(String[] args) {
        SecurityMaster securityMaster = new SecurityMaster();
        securityMaster.initliaze();
    }

    public void initliaze(){
        sendAsciiText();

        /**
         * ConfigurationFiles
         */
        ConfigurationFiles configurationFiles = new ConfigurationFiles();
        setConfigurationFiles(configurationFiles);

        /**
         * NetworkManager
         */
        NetworkManager networkManager = new NetworkManager();
        setNetworkManager(networkManager);

        /**
         * CommandManager
         */
        CommandManager commandManager = new CommandManager();
        setCommandManager(commandManager);

        /**
         * PlayerManager
         */
        PlayerManager playerManager = new PlayerManager();
        setPlayerManager(playerManager);

        /**
         * ActionManager
         */
        ActionManager actionManager = new ActionManager();
        setActionManager(actionManager);

        /**
         * AuthManager
         *
         *
         */
        AuthManager authManager = new AuthManager();
        setAuthManager(authManager);

        /**
         * ModuleLoader
         */
        ModuleLoader moduleLoader = new ModuleLoader();
        setModuleLoader(moduleLoader);
        moduleLoader.initalizeTimer();

        /**
         * EventManager
         */
        EventManager eventManager = new EventManager();
        setEventManager(eventManager);


        /**
         * Netty
         */
        startServers();

        /**
         * ShutdownHook
         */
        addShutdownHook();

    }



    private void sendAsciiText() {
        StringBuilder builder = new StringBuilder()
                .append(" _____                      _ _        ___  ___          _            ").append("\n")
                .append("/  ___|                    (_) |       |  \\/  |         | |           ").append("\n")
                .append("\\ `--.  ___  ___ _   _ _ __ _| |_ _   _| .  . | __ _ ___| |_ ___ _ __ ").append("\n")
                .append(" `--. \\/ _ \\/ __| | | | '__| | __| | | | |\\/| |/ _` / __| __/ _ \\ '__|").append("\n")
                .append("/\\__/ /  __/ (__| |_| | |  | | |_| |_| | |  | | (_| \\__ \\ ||  __/ |   ").append("\n")
                .append("\\____/ \\___|\\___|\\__,_|_|  |_|\\__|\\__, \\_|  |_/\\__,_|___/\\__\\___|_|   ").append("\n")
                .append("                                   __/ |                              ").append("\n")
                .append("                                  |___/                               ");
        String toString = builder.toString();
        System.out.println(toString);
    }

    public void exit(){
        System.exit(0);
    }

    public void addShutdownHook(){
        Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
            @Override
            public void run() {
                Logger.log(Logger.LoggerState.INFO, "SecurityMaster wird gestoppt.");
                NetworkManager.pool.shutdownNow();
                for(Module module : getModuleLoader().getLoadedModules()){
                    module.onDeactivate();
                }
            }
        }));
    }


    private void startServers(){
        setNettyConnection(new NettyConnection("localhost", 27777));
        new Thread(getNettyConnection()).start();
    }

    public SecurityProfiler getProfiler(){
        return SecurityProfiler.getInstance();
    }
}
