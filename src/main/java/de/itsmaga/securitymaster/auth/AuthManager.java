package de.itsmaga.securitymaster.auth;

import de.itsmaga.securitymaster.logger.Logger;
import de.itsmaga.securitymaster.network.NetworkManager;
import de.itsmaga.securitymaster.network.netty.NettyConnection;
import io.netty.channel.Channel;
import lombok.Getter;
import lombok.Setter;

import java.util.LinkedList;
import java.util.List;
import java.util.function.Consumer;

/**
 * Erstellt von ItsMaga
 */
@Getter
@Setter
public class AuthManager {

    private List<SecurityAuth> auths;
    private long time = System.currentTimeMillis() + 500;

    public AuthManager() {
        setAuths(new LinkedList<>());
        startTask();
    }

    private void startTask(){
        NetworkManager.pool.execute(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    if(System.currentTimeMillis() >= time){
                        for(SecurityAuth securityAuth : getAuths()){
                            if(System.currentTimeMillis() >= securityAuth.getTime()){
                                Logger.log(Logger.LoggerState.INFO, "Authentikation für ".concat(securityAuth.getChannel().channel().localAddress().toString()).concat(" ist fehlgeschlagen."));
                                NettyConnection.channels.remove(securityAuth.getChannel().channel());
                                securityAuth.getChannel().toMap().keySet().forEach(new Consumer<String>() {
                                    @Override
                                    public void accept(String s) {
                                        securityAuth.getChannel().remove(securityAuth.getChannel().toMap().get(s));
                                    }
                                });
                                getAuths().remove(securityAuth);
                            }
                        }

                        time = System.currentTimeMillis() + 500;
                    }
                }
            }
        });
    }


}
