package de.itsmaga.securitymaster.auth;

import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import lombok.Getter;


/**
 * Erstellt von ItsMaga
 */
@Getter

public class SecurityAuth {

    private ChannelPipeline channel;
    private long time;

    public SecurityAuth(ChannelPipeline channel) {
        this.channel = channel;
        this.time = System.currentTimeMillis() + 1000 * 5;
    }
}
