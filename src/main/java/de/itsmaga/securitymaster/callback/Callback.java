package de.itsmaga.securitymaster.callback;

/**
 * Erstellt von ItsMaga
 */
public interface Callback<T> {

    void invoke(T t);
}
