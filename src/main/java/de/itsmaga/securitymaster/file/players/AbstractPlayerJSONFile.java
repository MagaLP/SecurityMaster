package de.itsmaga.securitymaster.file.players;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import de.itsmaga.securitymaster.file.players.PlayersFile;
import de.itsmaga.securitymaster.players.PlayerManager;
import lombok.Getter;
import lombok.Setter;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * Erstellt von ItsMaga
 */
@Getter
@Setter
public abstract class AbstractPlayerJSONFile {

    private final static Gson gson = new GsonBuilder().setPrettyPrinting().create();
    private Map<String, Object> keys;
    private File file;

    public AbstractPlayerJSONFile(UUID uuid, String name, String password, String ipAddress) {
        String path = PlayerManager.PATH;
        File directory = new File(path);
        directory.mkdirs();
        setFile(new File(path + uuid.toString().concat(".json")));
        write(name, password, ipAddress);

    }

    public static PlayersFile load(File file){
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(file)));
            Map<String, Object> tags;
            tags = gson.fromJson(reader, HashMap.class);

            UUID uuid = UUID.fromString(file.getName().replace(".json", ""));

            return new PlayersFile(uuid, (String)tags.get("name"), (String)tags.get("password"), (String)tags.get("address"));
        }catch(FileNotFoundException ex){
            ex.printStackTrace();
        }
        return null;
    }

    private void write(String name, String password, String ipAddress){
        if(!getFile().exists()){
            try {
                getFile().createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
            setKeys(new HashMap<>());
            create(name, password, ipAddress);
            save();
        } else {
            try {
                BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(getFile())));
                setKeys(gson.fromJson(reader, HashMap.class));
            }catch(FileNotFoundException ex){
                ex.printStackTrace();
            }

        }
    }

    public void set(String key, Object object){
        getKeys().put(key, object);
    }

    public Object getObject(String key){
        if(!getKeys().containsKey(key)){
            return null;
        }
        return getKeys().get(key);
    }


    public void save(){
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        String jsonConfiguration = gson.toJson(getKeys());
        try {
            FileWriter writer = new FileWriter(getFile());
            writer.write(jsonConfiguration);
            writer.flush();
            writer.close();
        }catch(IOException e){
            e.printStackTrace();
        }
    }

    public abstract void create(String name, String password, String ipAddress);

}
