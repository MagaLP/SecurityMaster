package de.itsmaga.securitymaster.file.players;

import java.util.UUID;

/**
 * Erstellt von ItsMaga
 */
public class PlayersFile extends AbstractPlayerJSONFile {


    public final String nameField = "name";
    public final String passwordField = "password";
    public final String addressField = "address";


    public PlayersFile(UUID uuid, String name, String password, String ipAddress) {
        super(uuid, name, password, ipAddress);
    }

    @Override
    public void create(String name, String password, String ipAddress) {
        set(nameField, name);
        set(passwordField, password);
        set(addressField, ipAddress);
    }

    public String getName(){
        return (String)getKeys().get(nameField);
    }

    public String getPassword(){
        return (String)getKeys().get(passwordField);
    }

    public String getAddress(){
        return (String)getKeys().get(addressField);
    }
}
