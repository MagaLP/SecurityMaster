package de.itsmaga.securitymaster.file;

import de.itsmaga.securitymaster.file.config.AuthFile;
import lombok.Getter;
import lombok.Setter;

/**
 * Erstellt von ItsMaga
 */
@Getter
@Setter
public class ConfigurationFiles {

    /**
     * Files
     */
    private AuthFile authFile;

    public ConfigurationFiles() {
        /**
         * initalize Files
         */
        setAuthFile(new AuthFile());
    }
}
