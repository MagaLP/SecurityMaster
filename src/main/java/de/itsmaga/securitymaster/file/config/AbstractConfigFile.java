package de.itsmaga.securitymaster.file.config;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import lombok.Getter;
import lombok.Setter;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

/**
 * Erstellt von ItsMaga
 */
@Getter
@Setter
public abstract class AbstractConfigFile {

    private final static Gson gson = new GsonBuilder().setPrettyPrinting().create();
    private Map<String, Object> keys;
    private File file;

    public AbstractConfigFile(String path, String fileName) {
        File directory = new File(path);
        directory.mkdirs();
        setFile(new File(path + fileName));
        write();

    }



    private void write(){
        if(!getFile().exists()){
            try {
                getFile().createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
            setKeys(new HashMap<>());
            create();
            save();
        } else {
            try {
                BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(getFile())));
                setKeys(gson.fromJson(reader, HashMap.class));
            }catch(FileNotFoundException ex){
                ex.printStackTrace();
            }

        }
    }

    public void set(String key, Object object){
        getKeys().put(key, object);
    }

    public Object getObject(String key){
        if(!getKeys().containsKey(key)){
            return null;
        }
        return getKeys().get(key);
    }


    public void save(){
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        String jsonConfiguration = gson.toJson(getKeys());
        try {
            FileWriter writer = new FileWriter(getFile());
            writer.write(jsonConfiguration);
            writer.flush();
            writer.close();
        }catch(IOException e){
            e.printStackTrace();
        }
    }

    public abstract void create();

}
