package de.itsmaga.securitymaster.file.config;

/**
 * Erstellt von ItsMaga
 */
public class AuthFile extends AbstractConfigFile{

    private final String authField = "authenticationKey";

    public AuthFile() {
        super("configuration/", "auth.json");
    }

    @Override
    public void create() {
        set(authField, "none");
    }

    public String getAuthKey(){
        return (String)getObject(authField);
    }
}
