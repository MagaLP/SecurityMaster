package de.itsmaga.securitymaster.action.impl;

import de.itsmaga.securitylib.action.Action;
import de.itsmaga.securitylib.packet.PacketHolder;
import de.itsmaga.securitylib.packet.PacketType;
import de.itsmaga.securitylib.packet.types.AuthPacket;
import de.itsmaga.securitymaster.SecurityMaster;
import de.itsmaga.securitymaster.auth.SecurityAuth;
import de.itsmaga.securitymaster.logger.Logger;
import de.itsmaga.securitymaster.network.netty.NettyConnection;
import io.netty.channel.ChannelHandlerContext;
import sun.reflect.Reflection;


/**
 * Erstellt von ItsMaga
 */
public class AuthAction extends Action{

    public AuthAction() {
        super(PacketType.AUTH_PACKET);
    }

    @Override
    public void recieved(ChannelHandlerContext channelHandlerContext, Object object) {
        PacketHolder packetHolder = (PacketHolder)object;
        AuthPacket packet = (AuthPacket)packetHolder.getValue();

        String clientKey = packet.getKey();
        if(!clientKey.equals(SecurityMaster.getInstance().getConfigurationFiles().getAuthFile().getAuthKey())){
            for(SecurityAuth securityAuth : SecurityMaster.getInstance().getAuthManager().getAuths()){
                if(securityAuth.getChannel() == channelHandlerContext.pipeline()){
                    channelHandlerContext.pipeline().remove(channelHandlerContext.handler());
                    NettyConnection.channels.remove(channelHandlerContext.channel());
                    SecurityMaster.getInstance().getAuthManager().getAuths().remove(this);
                    Logger.log(Logger.LoggerState.WARNING, "Authentikation für ".concat(securityAuth.getChannel().channel().localAddress().toString()).concat(" ist fehlgeschlagen."));
                }
            }
        } else {
            for(SecurityAuth securityAuth : SecurityMaster.getInstance().getAuthManager().getAuths()){
                if(securityAuth.getChannel() == channelHandlerContext.pipeline()){
                    Logger.log(Logger.LoggerState.SUCCESS, "Authentikation für ".concat(securityAuth.getChannel().channel().localAddress().toString()).concat(" ist erfolgreich."));
                    SecurityMaster.getInstance().getAuthManager().getAuths().remove(securityAuth);
                }
            }

        }
    }
}
