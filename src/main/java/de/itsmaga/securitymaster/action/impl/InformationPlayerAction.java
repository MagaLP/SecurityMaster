package de.itsmaga.securitymaster.action.impl;

import de.itsmaga.securitylib.action.Action;
import de.itsmaga.securitylib.packet.PacketHolder;
import de.itsmaga.securitylib.packet.PacketType;
import de.itsmaga.securitylib.packet.types.InformationPlayerPacket;
import de.itsmaga.securitymaster.SecurityMaster;
import de.itsmaga.securitymaster.players.Player;
import de.itsmaga.securitymaster.players.PlayerManager;
import io.netty.channel.ChannelHandlerContext;

import java.util.UUID;

/**
 * Erstellt von ItsMaga
 */
public class InformationPlayerAction extends Action{

    public InformationPlayerAction() {
        super(PacketType.INFORMATION_PLAYER_PACKET);
    }

    @Override
    public void recieved(ChannelHandlerContext channelHandlerContext, Object object) {
        PacketHolder packetHolder = (PacketHolder)object;
        PlayerManager manager = SecurityMaster.getInstance().getPlayerManager();
        InformationPlayerPacket packet = (InformationPlayerPacket)packetHolder.getValue();
        UUID uuid = packet.getUuid();
        if(manager.existsPlayerByUUID(uuid)){
            Player player = manager.getPlayerByUUID(uuid);
            InformationPlayerPacket send = new InformationPlayerPacket(uuid, player.getName(), player.getPassword(), player.getAddress());
            channelHandlerContext.channel().writeAndFlush(SecurityMaster.getInstance().getNetworkManager().getPacketHelper().preparePacket(PacketType.INFORMATION_PLAYER_PACKET, send));
        }


    }
}
