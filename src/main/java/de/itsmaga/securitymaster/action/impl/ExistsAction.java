package de.itsmaga.securitymaster.action.impl;

import de.itsmaga.securitylib.action.Action;
import de.itsmaga.securitylib.packet.PacketHolder;
import de.itsmaga.securitylib.packet.PacketType;
import de.itsmaga.securitylib.packet.types.ExistsPacket;
import de.itsmaga.securitymaster.SecurityMaster;
import de.itsmaga.securitymaster.logger.Logger;
import de.itsmaga.securitymaster.players.Player;
import io.netty.channel.ChannelHandlerContext;

import java.util.UUID;

/**
 * Erstellt von ItsMaga
 */
public class ExistsAction extends Action{

    public ExistsAction() {
        super(PacketType.EXISTS_PACKET);
    }

    @Override
    public void recieved(ChannelHandlerContext channelHandlerContext, Object object) {
        PacketHolder packetHolder = (PacketHolder)object;
        ExistsPacket packet = (ExistsPacket)packetHolder.getValue();
        UUID uuid = packet.getUuid();
        boolean value = false;
        if(SecurityMaster.getInstance().getPlayerManager().existsPlayerByUUID(uuid)){
            value = true;
            Player player = SecurityMaster.getInstance().getPlayerManager().getPlayerByUUID(uuid);
            Logger.log(Logger.LoggerState.INFO, "Player ".concat(player.getName()).concat(" hat sich eingeloggt."));

        }
        ExistsPacket send = new ExistsPacket(value, uuid);
        channelHandlerContext.channel().writeAndFlush(SecurityMaster.getInstance().getNetworkManager().getPacketHelper().preparePacket(PacketType.EXISTS_PACKET, send));
    }
}
