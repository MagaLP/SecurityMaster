package de.itsmaga.securitymaster.action.impl;

import de.itsmaga.securitylib.action.Action;
import de.itsmaga.securitylib.packet.PacketHolder;
import de.itsmaga.securitylib.packet.PacketType;
import de.itsmaga.securitylib.packet.types.ChangeAddressPacket;
import de.itsmaga.securitylib.packet.types.ChangeNamePacket;
import de.itsmaga.securitymaster.SecurityMaster;
import de.itsmaga.securitymaster.logger.Logger;
import de.itsmaga.securitymaster.players.Player;
import de.itsmaga.securitymaster.players.PlayerManager;
import io.netty.channel.ChannelHandlerContext;

import java.util.UUID;

/**
 * Erstellt von ItsMaga
 */
public class ChangeNameAction extends Action{

    public ChangeNameAction() {
        super(PacketType.CHANGE_NAME);
    }

    @Override
    public void recieved(ChannelHandlerContext channelHandlerContext, Object object) {
        PacketHolder packetHolder = (PacketHolder)object;
        PlayerManager manager = SecurityMaster.getInstance().getPlayerManager();
        ChangeNamePacket packet = (ChangeNamePacket) packetHolder.getValue();
        UUID uuid = packet.getUuid();
        if(manager.existsPlayerByUUID(uuid)){
            String toChange = packet.getName();
            Player player = manager.getPlayerByUUID(uuid);
            player.setName(toChange);
            Logger.log(Logger.LoggerState.INFO, "Player ".concat(uuid.toString()).concat(" Name wurde geupdated."));
        }

    }
}
