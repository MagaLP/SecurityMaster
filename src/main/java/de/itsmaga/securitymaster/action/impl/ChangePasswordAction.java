package de.itsmaga.securitymaster.action.impl;

import de.itsmaga.securitylib.action.Action;
import de.itsmaga.securitylib.packet.PacketHolder;
import de.itsmaga.securitylib.packet.PacketType;
import de.itsmaga.securitylib.packet.types.ChangePasswordPacket;
import de.itsmaga.securitymaster.SecurityMaster;
import de.itsmaga.securitymaster.logger.Logger;
import de.itsmaga.securitymaster.players.Player;
import io.netty.channel.ChannelHandlerContext;

/**
 * Erstellt von ItsMaga
 */
public class ChangePasswordAction extends Action{

    public ChangePasswordAction() {
        super(PacketType.CHANGE_PASSWORD);
    }

    @Override
    public void recieved(ChannelHandlerContext channelHandlerContext, Object object) {
        PacketHolder packetHolder = (PacketHolder)object;
        ChangePasswordPacket packet = (ChangePasswordPacket)packetHolder.getValue();
        Player player = SecurityMaster.getInstance().getPlayerManager().getPlayerByUUID(packet.getUuid());
        player.setPassword(packet.getPassword());
        Logger.log(Logger.LoggerState.INFO, "Player ".concat(player.getName()).concat(" hat sein Password geändert."));

    }
}
