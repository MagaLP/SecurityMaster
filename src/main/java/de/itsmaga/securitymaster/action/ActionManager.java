package de.itsmaga.securitymaster.action;

import de.itsmaga.securitylib.action.AbstractActionManager;
import de.itsmaga.securitymaster.action.impl.*;

/**
 * Erstellt von ItsMaga
 */
public class ActionManager extends AbstractActionManager{


    @Override
    public void initalizeList() {
        getActions().add(new AuthAction());
        getActions().add(new ExistsAction());
        getActions().add(new InformationPlayerAction());
        getActions().add(new ChangeAddressAction());
        getActions().add(new ChangeNameAction());
        getActions().add(new ChangePasswordAction());
    }


}
