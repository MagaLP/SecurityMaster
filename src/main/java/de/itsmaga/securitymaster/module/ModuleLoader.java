package de.itsmaga.securitymaster.module;

import de.itsmaga.securitymaster.SecurityMaster;
import de.itsmaga.securitymaster.logger.Logger;
import de.itsmaga.securitymaster.utils.SecurityProfiler;
import org.apache.commons.codec.digest.DigestUtils;
import lombok.Getter;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Erstellt von ItsMaga
 */

@Getter
public class ModuleLoader {

    private final String keyProfile = "ModuleLoader";
    private List<Module> loadedModules = new ArrayList<>();
    private List<String> hashModules = new ArrayList<>();


    public void load(File dir, ClassLoader loader) throws IOException {
        SecurityProfiler.Profile profile = SecurityMaster.getInstance().getProfiler().start(keyProfile);
        dir.mkdirs();

        List<File> newFiles = Arrays.stream(dir.listFiles((dir1, name) -> name.endsWith(".jar"))).filter(file -> {
            try (FileInputStream fis = new FileInputStream(file)) {
                String md5Hash = DigestUtils.md5Hex(fis);
                boolean contains = hashModules.contains(md5Hash);
                if (!contains)
                    hashModules.add(md5Hash);
                return !contains;
            } catch (IOException e) {
                Logger.log(Logger.LoggerState.WARNING, "Fehler : " + e.getMessage());
            }
            return false;
        }).collect(Collectors.toList());

        URL[] urls = new URL[newFiles.size()];
        for (int i = 0; i < newFiles.size(); i++)
            urls[i] = newFiles.get(i).toURI().toURL();
        URLClassLoader ucl = new URLClassLoader(urls, loader);

        ServiceLoader<Module> serviceLoader = ServiceLoader.load(Module.class, ucl);
        Iterator<Module> iterator = serviceLoader.iterator();


        try {
            while (iterator.hasNext()) {
                Module module = iterator.next();
                Module oldModule = loadedModules.stream().filter(module1 -> module1.getName().equals(module.getName())).findAny().orElse(null);
                if (oldModule != null) {
                    try {
                        oldModule.onDeactivate();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                    loadedModules.remove(oldModule);
                }
                loadedModules.add(module);
                Logger.log(Logger.LoggerState.WARNING, "Module (Name: " +module.getName()+", Author: "+module.getAuthor()+", Version: "+module.getVersion()+") loaded");
                try {
                    module.onActivate();
                } catch (Exception ex) {
                    Logger.log(Logger.LoggerState.WARNING, "Fehler : " + ex.getMessage());
                }
            }
        } catch (Exception e) {
            Logger.log(Logger.LoggerState.WARNING, "Fehler : " + e.getMessage());
        }
       SecurityMaster.getInstance().getProfiler().stop(profile);
    }



    public void initalizeTimer(){
        File file = new File("modules/");
        if(!file.exists()){
            file.mkdir();
        }
        Logger.log(Logger.LoggerState.INFO, "Load modules file ("+file.getAbsolutePath()+")");

        ClassLoader loader = SecurityMaster.class.getClassLoader();
        try {
            load(file, loader);
            Logger.log(Logger.LoggerState.INFO, "Loaded ("+loadedModules.size()+") modules");
        }catch(IOException ex){
            Logger.log(Logger.LoggerState.WARNING, "Fehler : " + ex.getMessage());
        }
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                try {
                    load(file, loader);
                }catch(IOException ex){
                    Logger.log(Logger.LoggerState.WARNING, "Fehler : " + ex.getMessage());
                }
            }
        }, 30_000, 30_000);
    }
}
