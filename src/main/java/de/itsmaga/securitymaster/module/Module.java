package de.itsmaga.securitymaster.module;

import java.io.File;

/**
 * Erstellt von ItsMaga
 */
public interface Module {

    String getName();

    String getVersion();

    String getAuthor();

    void onActivate();

    void onDeactivate();

    File getModuleDirectory();
}
