package de.itsmaga.securitymaster.module;

import java.io.File;

/**
 * Erstellt von ItsMaga
 */
public abstract class AbstractModule implements Module {

    private File file = new File("modules/".concat(getName()).concat("/"));


    @Override
    public void onActivate() {

    }

    @Override
    public void onDeactivate() {

    }

    @Override
    public File getModuleDirectory() {
        file.mkdirs();
        return file;
    }

    @Override
    public boolean equals(Object obj) {
        if(!(obj instanceof Module)){
            return false;
        }
        Module module = (Module)obj;
        return getName().equals(module.getName()) && getVersion().equals(module.getVersion())
                && getAuthor().equals(module.getAuthor());
    }
}
