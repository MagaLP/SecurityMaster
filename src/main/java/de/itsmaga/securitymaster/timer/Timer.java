package de.itsmaga.securitymaster.timer;

/**
 * Erstellt von ItsMaga
 */
public class Timer{

    private String name;
    private Thread thread;
    private long time;
    private long waitTime;

    private boolean running;

    public Timer(String name, long timeInMillie) {
        this.name = name;
        time = System.currentTimeMillis();
        this.waitTime = timeInMillie;
        this.running = true;
    }

    public Timer startTimer(TimerInterface timerInterface, boolean daemon) {
        this.thread = new Thread(new Runnable() {
            @Override
            public void run() {
                while (isRunning()){
                    if(System.currentTimeMillis() >= time){
                        timerInterface.onRun();
                        time = System.currentTimeMillis() + waitTime;
                    }
                }

            }
        }, name);
        this.thread.setDaemon(daemon);
        this.thread.start();
        return this;
    }

    public void setRunning(boolean running) {
        this.running = running;
    }

    public boolean isDaemon(){
        return thread.isDaemon();
    }

    public void cancel() {
        this.running = false;
        TimerManager.getInstance().getTimers().remove(name);
    }

    public String getName() {
        return name;
    }

    public boolean isRunning() {
        return running;
    }

    public long getWaitTime() {
        return waitTime;
    }

    public Thread getThread() {
        return thread;
    }
}
