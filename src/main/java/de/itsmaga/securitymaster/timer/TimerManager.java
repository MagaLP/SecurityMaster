package de.itsmaga.securitymaster.timer;

import java.util.concurrent.ConcurrentHashMap;

/**
 * Erstellt von ItsMaga
 */
public class TimerManager {

    private ConcurrentHashMap<String, Timer> timers = new ConcurrentHashMap<>();
    private static TimerManager instance;



    public void startTimer(Timer timer){
        timers.put(timer.getName(), timer);
    }

    public void disable(){
        for(Timer timer : timers.values()){
            timer.cancel();
        }
    }

    public void stopTimer(String key){
        if(!timers.containsKey(key)){
            return;
        }
        Timer timer = timers.get(key);
        timer.setRunning(false);
        timers.remove(key);
    }

    public ConcurrentHashMap<String, Timer> getTimers() {
        return timers;
    }

    public static TimerManager getInstance() {
        return instance == null ? new TimerManager() : instance;
    }
}
