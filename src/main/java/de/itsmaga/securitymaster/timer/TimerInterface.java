package de.itsmaga.securitymaster.timer;

/**
 * Erstellt von ItsMaga
 */
public interface TimerInterface {

    void onRun();
}
