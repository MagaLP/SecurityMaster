package de.itsmaga.securitymaster.commander.impl;

import de.itsmaga.securitymaster.SecurityMaster;
import de.itsmaga.securitymaster.commander.Command;
import de.itsmaga.securitymaster.file.players.PlayersFile;
import de.itsmaga.securitymaster.logger.Logger;
import de.itsmaga.securitymaster.players.Player;
import de.itsmaga.securitymaster.players.PlayerManager;

import java.net.InetSocketAddress;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.UUID;


/**
 * Erstellt von ItsMaga
 */
public class AddPlayerCommand extends Command{



    public AddPlayerCommand() {
        super("addplayer", "addplayers");
    }

    @Override
    public void execute(String[] args) {
        if(args.length == 5){
            String playerName = args[1];
            PlayerManager playerManager = SecurityMaster.getInstance().getPlayerManager();
            if(playerManager.existsPlayerByName(playerName)){
                Logger.log(Logger.LoggerState.WARNING, "Dieses Teammitglied existiert bereits.");
                return;
            }
            UUID uuid = UUID.fromString(args[2]);
            if(uuid == null){
                Logger.log(Logger.LoggerState.WARNING, "Diese UUID ist nicht verfügbar.");
               return;
            }
            String password = args[3];
            String ipAddress = args[4];

            String hash = "";
            try {
                hash = new String(MessageDigest.getInstance("MD5").digest(password.getBytes()));
            }catch(NoSuchAlgorithmException ex){
                ex.printStackTrace();
            }

            Player player = new Player(new PlayersFile(uuid, playerName, hash, ipAddress));
            playerManager.getPlayers().add(player);
            Logger.log(Logger.LoggerState.INFO, "Du hast erfolgreich ein Teammitglied hinzugefügt.");
            Logger.log(Logger.LoggerState.INFO, "UUID: " + uuid.toString());
            Logger.log(Logger.LoggerState.INFO, "Name: " + playerName);
            Logger.log(Logger.LoggerState.INFO, "Password: " + hash);
            Logger.log(Logger.LoggerState.INFO, "Address: " + ipAddress);
        } else {
            Logger.log(Logger.LoggerState.INFO,"Bitte gebe 'help' ein, um die Befehle zu sehen.");
        }


    }
}
