package de.itsmaga.securitymaster.commander.impl;

import de.itsmaga.securitylib.packet.PacketType;
import de.itsmaga.securitylib.packet.types.PlayerLoginPacket;
import de.itsmaga.securitymaster.SecurityMaster;
import de.itsmaga.securitymaster.commander.Command;
import de.itsmaga.securitymaster.logger.Logger;
import de.itsmaga.securitymaster.network.netty.NettyConnection;
import de.itsmaga.securitymaster.players.Player;
import de.itsmaga.securitymaster.players.PlayerManager;
import io.netty.channel.Channel;

import java.util.UUID;

/**
 * Erstellt von ItsMaga
 */
public class TestCommand extends Command{

    public TestCommand() {
        super("test", "test");
    }

    @Override
    public void execute(String[] args) {
        for(Channel channel : NettyConnection.channels){
            channel.writeAndFlush(SecurityMaster.getInstance().getNetworkManager().getPacketHelper().preparePacket(PacketType.PLAYER_LOGIN_PACKET, new PlayerLoginPacket(UUID.fromString("25430ff9-dc11-45b5-8f6a-d83a9fd1103a"))));
        }

    }
}
