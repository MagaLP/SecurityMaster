package de.itsmaga.securitymaster.commander.impl;

import de.itsmaga.securitymaster.SecurityMaster;
import de.itsmaga.securitymaster.commander.Command;
import de.itsmaga.securitymaster.file.players.PlayersFile;
import de.itsmaga.securitymaster.logger.Logger;
import de.itsmaga.securitymaster.players.Player;
import de.itsmaga.securitymaster.players.PlayerManager;

import java.util.UUID;

/**
 * Erstellt von ItsMaga
 */
public class RemovePlayerCommand extends Command{


    public RemovePlayerCommand() {
        super("removePlayer", "removeplayers");
    }

    @Override
    public void execute(String[] args) {
        if(args.length == 2){
            String playerName = args[1];
            PlayerManager playerManager = SecurityMaster.getInstance().getPlayerManager();
            if(!playerManager.existsPlayerByName(playerName)){
                Logger.log(Logger.LoggerState.WARNING, "Dieses Teammitglied existiert nicht.");
                return;
            }
            Player player = playerManager.getPlayerByName(playerName);
            Logger.log(Logger.LoggerState.INFO, "Du hast erfolgreich das Teammitglied ".concat(player.getName()).concat(" entfernt."));
            playerManager.deletePlayer(player);


        } else {
            Logger.log(Logger.LoggerState.INFO,"Bitte gebe 'help' ein, um die Befehle zu sehen.");
        }


    }
}
