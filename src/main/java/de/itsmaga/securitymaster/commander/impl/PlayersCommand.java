package de.itsmaga.securitymaster.commander.impl;

import de.itsmaga.securitymaster.SecurityMaster;
import de.itsmaga.securitymaster.commander.Command;
import de.itsmaga.securitymaster.logger.Logger;
import de.itsmaga.securitymaster.players.Player;
import de.itsmaga.securitymaster.players.PlayerManager;

/**
 * Erstellt von ItsMaga
 */
public class PlayersCommand extends Command{

    public PlayersCommand() {
        super("players", "players");
    }

    @Override
    public void execute(String[] args) {
        PlayerManager manager = SecurityMaster.getInstance().getPlayerManager();
        if(manager.getPlayers().isEmpty()){
            Logger.log(Logger.LoggerState.INFO, "Es existieren keine Teammitglieder derzeit.");
            return;
        }
        Logger.log(Logger.LoggerState.INFO, "Teammitglieder:");
        for(Player player : manager.getPlayers()){
            Logger.log(Logger.LoggerState.INFO, " - ".concat(player.getName()));
        }

    }
}
