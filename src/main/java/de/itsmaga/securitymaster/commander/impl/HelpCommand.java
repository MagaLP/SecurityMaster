package de.itsmaga.securitymaster.commander.impl;

import de.itsmaga.securitymaster.commander.Command;
import de.itsmaga.securitymaster.logger.Logger;

/**
 * Erstellt von ItsMaga
 */
public class HelpCommand extends Command{

    public HelpCommand() {
        super("help", "hilfe");
    }

    @Override
    public void execute(String[] args) {
        Logger.log(Logger.LoggerState.INFO, "Befehle:");
        Logger.log(Logger.LoggerState.INFO, "   help | Liste aus Befehlen");
        Logger.log(Logger.LoggerState.INFO, "   stop | Stoppst du den SecurityMaster");
        Logger.log(Logger.LoggerState.INFO, "   addPlayer <PlayerName> <UUID> <Passwort> <Address> | Registriere ein Teammitglied");
        Logger.log(Logger.LoggerState.INFO, "   removePlayer <PlayerName> | Entferne ein Teammitglied");
        Logger.log(Logger.LoggerState.INFO, "   players | Liste aus registrierten Teammitglieder");
        Logger.log(Logger.LoggerState.INFO, "   playerlog <PlayerName> | Aktivitäten eines Teammitglieds");
        Logger.log(Logger.LoggerState.INFO, "   clear | Leere die Console");

    }
}
