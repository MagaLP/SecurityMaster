package de.itsmaga.securitymaster.commander.impl;

import de.itsmaga.securitymaster.commander.Command;


/**
 * Erstellt von ItsMaga
 */
public class ClearCommand extends Command{

    private final int size = 60;

    public ClearCommand() {
        super("clear", "clearconsole");
    }

    @Override
    public void execute(String[] args) {
        if(args.length == 0){
            StringBuilder builder = new StringBuilder("");
            for (int i = 0; i < size; i++) {
                builder = builder.append("\n");
            }
            System.out.println(builder.toString());
        } else {
            sendHelp();
        }
    }
}
