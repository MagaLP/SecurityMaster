package de.itsmaga.securitymaster.commander.impl;

import de.itsmaga.securitymaster.SecurityMaster;
import de.itsmaga.securitymaster.commander.Command;
import de.itsmaga.securitymaster.logger.Logger;

/**
 * Erstellt von ItsMaga
 */
public class StopCommand extends Command{

    public StopCommand() {
        super("stop", "close");
    }

    @Override
    public void execute(String[] args) {
        SecurityMaster.getInstance().exit();
    }
}
