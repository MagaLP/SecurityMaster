package de.itsmaga.securitymaster.commander;

import de.itsmaga.securitymaster.commander.impl.*;
import de.itsmaga.securitymaster.logger.Logger;
import de.itsmaga.securitymaster.network.NetworkManager;
import lombok.Getter;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Erstellt von ItsMaga
 */
@Getter
public class CommandManager {

    private BufferedReader reader;
    public List<Command> commands = new ArrayList<>();

    public CommandManager() {
        initalize();
    }



    private void initalize(){
        this.reader = new BufferedReader(new InputStreamReader(System.in));
        registerCommands();
        start();

    }


    private void registerCommands() {
        Logger.log(Logger.LoggerState.INFO, "Registriere commands.");
        this.getCommands().add(new HelpCommand());
        this.getCommands().add(new StopCommand());
        this.getCommands().add(new PlayersCommand());
        this.getCommands().add(new AddPlayerCommand());
        this.getCommands().add(new RemovePlayerCommand());
        this.getCommands().add(new PlayerInfoCommand());
        this.getCommands().add(new TestCommand());
        this.getCommands().add(new ClearCommand());
    }

    private void start() {
        NetworkManager.pool.execute(() -> {
            while (true){
                try {
                    String line = "";
                    line = reader.readLine();

                    if(line.isEmpty()){
                        Logger.log(Logger.LoggerState.INFO, "Bitte gebe einen Text ein.");
                        continue;
                    }
                    String[] split = line.split(" ");
                    boolean done = false;
                    for(Command command : getCommands()){
                        if(command.getName().equalsIgnoreCase(split[0]) || isSame(command.getAliases(), split[0])) {
                            command.execute(split);
                            done = true;
                        }
                    }
                    if(done == false){
                        Logger.log(Logger.LoggerState.INFO,"Bitte gebe 'help' ein, um die Befehle zu sehen.");
                    }
                }catch(IOException ex){
                    ex.printStackTrace();
                }

            }
        });
    }



    private boolean isSame(List<String> aliases, String text){
        for(String key : aliases){
            if(key.equalsIgnoreCase(text)){
                return true;
            }
        }
        return false;
    }
}
