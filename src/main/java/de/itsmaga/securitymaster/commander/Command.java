package de.itsmaga.securitymaster.commander;

import de.itsmaga.securitymaster.logger.Logger;
import lombok.Getter;

import java.util.Arrays;
import java.util.List;

/**
 * Erstellt von ItsMaga
 */
@Getter
public abstract class Command {

    private String name;
    private List<String> aliases;

    public Command(String name, String... aliases) {
        this.name = name;
        this.aliases = Arrays.asList(aliases);
    }

    public abstract void execute(String[] args);

    public void sendHelp(){
        Logger.log(Logger.LoggerState.INFO,"Bitte gebe 'help' ein, um die Befehle zu sehen.");
    }


}
