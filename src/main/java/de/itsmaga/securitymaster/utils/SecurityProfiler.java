package de.itsmaga.securitymaster.utils;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Erstellt von ItsMaga
 */
public class SecurityProfiler {

    @Getter private static SecurityProfiler instance = new SecurityProfiler();
    @Getter private final List<Profile> profiles = Collections.synchronizedList(new ArrayList<>());


    public Profile start(String key){
        return new Profile(key, System.currentTimeMillis());
    }

    public void stop(Profile profile){
        if(profile == null){
            return;
        }
        profile.time = System.currentTimeMillis() - profile.getTime();
        profiles.add(profile);
    }


    @Getter
    @AllArgsConstructor(access = AccessLevel.PROTECTED)
    public static class Profile {

        private final String key;
        private long time;
    }
}
