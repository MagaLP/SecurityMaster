package de.itsmaga.securitymaster.event.impl;

import de.itsmaga.securitylib.packet.PacketType;
import de.itsmaga.securitymaster.event.Event;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Erstellt von ItsMaga
 */
@Getter
@AllArgsConstructor
public class PacketRecieveEvent implements Event{

    private PacketType type;
    private Object object;

}
