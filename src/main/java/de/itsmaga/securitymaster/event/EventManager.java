package de.itsmaga.securitymaster.event;

import de.itsmaga.securitymaster.SecurityMaster;
import de.itsmaga.securitymaster.utils.SecurityProfiler;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Erstellt von ItsMaga
 */
public class EventManager {

    public final String eventCallPrefix = "EventHandler_callEvent_";

    private final Map<Class<? extends Event>, Set<EventHandler>> eventHandlers = new HashMap<>();

    public void registerEventHandler(Object object){
        for(Method method : object.getClass().getDeclaredMethods()){
            if(method.getGenericParameterTypes().length == 1 && method.getAnnotation(Listener.class) != null){
                Class clazz = method.getParameters()[0].getType();
                if(clazz.getInterfaces()[0].equals(Event.class)){
                    if(!this.eventHandlers.containsKey(clazz)){
                        this.eventHandlers.put(clazz, new HashSet<>());
                    }
                    this.eventHandlers.get(clazz).add(new EventHandler(object, method));
                }
            }
        }
    }

    public void callEvent(Event event){
        Set<EventHandler> eventSet = this.eventHandlers.get(event.getClass());
        if(eventSet == null) return;
        for(EventHandler eventHandler : eventSet){
            SecurityProfiler.Profile profile = SecurityMaster.getInstance().getProfiler().start(eventCallPrefix.concat(eventHandler.getClass().getSimpleName()));
            try {
                eventHandler.method.invoke(eventHandler.instance, event);
            }catch(Exception ex){
                ex.printStackTrace();
            }
            SecurityMaster.getInstance().getProfiler().stop(profile);
        }
    }




    @RequiredArgsConstructor
    @Getter
    private static class EventHandler {
        private final Object instance;
        private final Method method;
    }
}
