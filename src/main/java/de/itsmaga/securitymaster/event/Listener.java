package de.itsmaga.securitymaster.event;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Erstellt von ItsMaga
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface Listener {

}
