package de.itsmaga.securitymaster.network;

import de.itsmaga.securitylib.packet.PacketHelper;
import lombok.Getter;
import lombok.Setter;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Erstellt von ItsMaga
 */
@Getter
@Setter
public class NetworkManager {

    public static ExecutorService pool = Executors.newCachedThreadPool();
    private PacketHelper packetHelper;

    public NetworkManager() {
        setPacketHelper(new PacketHelper());
    }
}
