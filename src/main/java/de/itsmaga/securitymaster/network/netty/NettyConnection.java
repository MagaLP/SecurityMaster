package de.itsmaga.securitymaster.network.netty;

import de.itsmaga.securitymaster.SecurityMaster;
import de.itsmaga.securitymaster.auth.SecurityAuth;
import de.itsmaga.securitymaster.logger.Logger;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.serialization.ClassResolvers;
import io.netty.handler.codec.serialization.ObjectDecoder;
import io.netty.handler.codec.serialization.ObjectEncoder;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;


/**
 * Erstellt von ItsMaga
 */
@Getter
@AllArgsConstructor
public class NettyConnection implements Runnable{

    private String host;
    private int port;
    public static List<Channel> channels = new ArrayList<>();


    @Override
    public void run() {
        Logger.log(Logger.LoggerState.INFO, "Starte Netty connection.");
        EventLoopGroup masterGroup = new NioEventLoopGroup();
        EventLoopGroup workerGroup = new NioEventLoopGroup();
        try {
            ServerBootstrap serverBootstrap = new ServerBootstrap()
                    .group(masterGroup, workerGroup)
                    .channel(NioServerSocketChannel.class)
                    .childHandler(new ChannelInitializer<SocketChannel>() {
                        @Override
                        protected void initChannel(SocketChannel socketChannel) throws Exception {
                            ChannelPipeline pipeline = socketChannel.pipeline();
                            pipeline.addLast(new ObjectDecoder(Integer.MAX_VALUE, ClassResolvers.cacheDisabled(this.getClass().getClassLoader())));
                            pipeline.addLast(new ObjectEncoder());
                            pipeline.addLast(new NettyHandler());
                            Logger.log(Logger.LoggerState.SUCCESS, "Der Client "
                                    .concat(socketChannel.localAddress().getHostString())
                                    .concat(":" + socketChannel.localAddress().getPort())
                                    .concat(" hat sich Verbunden."));
                            Logger.log(Logger.LoggerState.INFO, "Überprüfe Authentikation");
                            SecurityAuth auth = new SecurityAuth(socketChannel.pipeline());
                            SecurityMaster.getInstance().getAuthManager().getAuths().add(auth);

                            channels.add(pipeline.channel());

                        }
                    });
            ChannelFuture future = serverBootstrap.bind(getHost(), getPort()).sync();
            future.channel().closeFuture().sync();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            masterGroup.shutdownGracefully();
            workerGroup.shutdownGracefully();
        }
    }
}
