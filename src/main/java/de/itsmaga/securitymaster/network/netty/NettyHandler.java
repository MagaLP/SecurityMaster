package de.itsmaga.securitymaster.network.netty;

import de.itsmaga.securitylib.action.Action;
import de.itsmaga.securitylib.packet.PacketHolder;
import de.itsmaga.securitylib.packet.PacketType;
import de.itsmaga.securitymaster.SecurityMaster;
import de.itsmaga.securitymaster.event.impl.PacketRecieveEvent;
import de.itsmaga.securitymaster.logger.Logger;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

/**
 * Erstellt von ItsMaga
 */
public class NettyHandler extends SimpleChannelInboundHandler<Object>{



    @Override
    protected void channelRead0(ChannelHandlerContext channelHandlerContext, Object object) throws Exception {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                PacketHolder holder = (PacketHolder)object;
                for(Action action : SecurityMaster.getInstance().getActionManager().getActions()){
                    if(holder.getKey().equals(action.getType().getKey())){
                        action.recieved(channelHandlerContext, object);
                        SecurityMaster.getInstance().getEventManager().callEvent(new PacketRecieveEvent(holder.getType(), holder.getValue()));
                    }
                }


            }
        });
        thread.start();
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        cause.printStackTrace();
    }
}
