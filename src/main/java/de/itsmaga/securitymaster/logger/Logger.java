package de.itsmaga.securitymaster.logger;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Erstellt von ItsMaga
 */
public class Logger {

    public static void log(LoggerState state, String message) {
        System.out.println(state.getPrefix().concat(message));
    }


    @Getter
    @AllArgsConstructor
    public enum LoggerState {

        INFO("Info > "),
        WARNING("Warning > "),
        SUCCESS("Success > ");

        private String prefix;
    }
}
