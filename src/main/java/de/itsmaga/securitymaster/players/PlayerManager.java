package de.itsmaga.securitymaster.players;

import de.itsmaga.securitymaster.file.players.AbstractPlayerJSONFile;
import de.itsmaga.securitymaster.logger.Logger;
import lombok.Getter;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * Erstellt von ItsMaga
 */
@Getter
public class PlayerManager {

    public static final String PATH = "players/";

    private List<Player> players = new ArrayList<>();

    public PlayerManager() {
        load();
    }

    private void load(){
        File direcoty = new File(PATH);
        direcoty.mkdirs();



        Logger.log(Logger.LoggerState.INFO, "Load players");
        try {
            List<File> filesInFolder = Files.walk(Paths.get(PATH))
                    .filter(Files::isRegularFile)
                    .map(Path::toFile)
                    .collect(Collectors.toList());
            if(filesInFolder.isEmpty()){
                Logger.log(Logger.LoggerState.INFO, "Loaded (0) players");
                return;
            }
            int amount = 0;
            for(File file : filesInFolder){
                Player player = new Player(AbstractPlayerJSONFile.load(file));
                getPlayers().add(player);
                amount++;
            }
            Logger.log(Logger.LoggerState.INFO, "Loaded (" + amount + ") players");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Player getPlayerByUUID(UUID uuid){
        for(Player player : getPlayers()){
            if(player.getUUID() == uuid || uuid.toString().equals(player.getUUID().toString())){
                return player;
            }
        }
        return null;
    }

    public boolean existsPlayerByUUID(UUID uuid){
        for(Player player : getPlayers()){
            if(player.getUUID() == uuid || uuid.toString().equals(player.getUUID().toString())){
                return true;
            }
        }
        return false;
    }

    public void deletePlayer(Player player){
        File file = player.getPlayersFile().getFile();
        file.delete();
        getPlayers().remove(player);
    }

    public Player getPlayerByName(String name){
        for(Player player : getPlayers()){
            if(player.getName().equalsIgnoreCase(name)){
                return player;
            }
        }
        return null;
    }

    public boolean existsPlayerByName(String name){
        for(Player player : getPlayers()){
            if(player.getName().equalsIgnoreCase(name)){
                return true;
            }
        }
        return false;
    }


}
