package de.itsmaga.securitymaster.players;

import de.itsmaga.securitymaster.file.players.PlayersFile;
import lombok.Getter;

import java.util.UUID;

/**
 * Erstellt von ItsMaga
 */
@Getter
public class Player {

    private PlayersFile playersFile;

    public Player(PlayersFile playersFile) {
        this.playersFile = playersFile;
    }

    public String getName(){
        return getPlayersFile().getName();
    }

    public String getPassword(){
        return getPlayersFile().getPassword();
    }

    public UUID getUUID(){
        return UUID.fromString(getPlayersFile().getFile().getName().replace(".json", ""));
    }

    public String getAddress(){
        return getPlayersFile().getAddress();
    }

    public void setName(String name){
        getPlayersFile().set(getPlayersFile().nameField, name);
        getPlayersFile().save();
    }

    public void setPassword(String value){
        getPlayersFile().set(getPlayersFile().passwordField, value);
        getPlayersFile().save();
    }

    public void setAddress(String value){
        getPlayersFile().set(getPlayersFile().addressField, value);
        getPlayersFile().save();
    }





}
